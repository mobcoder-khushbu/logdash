import React, { Component } from 'react';
import {Text,
 Image, 
 View,
 TextInput,
  TouchableOpacity,StyleSheet, ScrollView} from 'react-native'; 
import Dashboard from './Dashboard';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

  
    function Login ({navigation}){
        return(
          
            <View style={styles.container}>
            <Text style={styles.login}>Login</Text>
             <TextInput
             style={styles.input}
             placeholder="Email"
             onChangeText={TextInputEmail => this.setState({ TextInputEmail })}
             underlineColorAndroid='black'
             />
            <TextInput
            style={styles.input}
            placeholder="Password"
            onChangeText={TextInputPassward => this.setState({ TextInputPassward })}
            underlineColorAndroid='black'
            secureTextEntry
            />
           <View style={styles.btnContainer}>
            <TouchableOpacity
            style={styles.userBtn}
            
            onPress={() => navigation.navigate('Dash')}>
            <Text style={styles.btnTxt}>Login</Text>
            </TouchableOpacity>
          
            </View>
                
            </View>
            
        );
}
export default Login;
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection:'column',
    
  },
  login: {
    fontSize: 40,
    textAlign: 'center',
    margin: 10,
    color: 'black',
    fontFamily: 'NotoSerif-Bold',
  },
  input: {
    width: '90%',
    backgroundColor: '#fff',
    padding: 15,
    marginBottom: 10,
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
  },
  userBtn: {
    backgroundColor: 'grey',
    padding: 15,
    width: '45%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnTxt: {
    fontSize: 22,
    textAlign: 'center',
  },
});
