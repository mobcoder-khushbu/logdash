import React, { Component } from 'react';
import {Image, View,StyleSheet, ScrollView} from 'react-native';
import Login from './Login';


export default class Background extends Component{
   

    render(){
        return(
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
                <Image source={require('./Images/login.jpg')} style={styles.imgBack}/>

                <Login />
            </View>
            </ScrollView>
            
            

        );
    }
}

 const styles=StyleSheet.create({
 
 container:{
     
   
 },
 imgBack:{
      width:'100%',
    height:300,
    resizeMode: 'cover',
    borderRadius:30
 },

 });