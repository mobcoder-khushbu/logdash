
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Dashboard from './component/Dashboard';
import Background from './component/Background';
function HomeScreen() {

    return(
      <View>
        <Background/>
      </View>
    );
  
}

const Stack = createStackNavigator();
function App() {
  return (
  <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Dash" component={Dashboard} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;