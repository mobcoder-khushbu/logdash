/**
 * @format
 */

import {AppRegistry} from 'react-native';
//import App from './App';
import {name as appName} from './app.json';
import Drawer from './component/Drawer';
AppRegistry.registerComponent(appName, () => Drawer);
